# Axelrod

Axelrod: Opis

Biblioteka do Pythona umożliwiająca iterowanie w łatwy sposób strategii z dziedziny dylematu więźnia.

Biblioteka obecnie wspiera ponad 100 różnych stategii.

Biblioteka potrafi przedstawiać wyniki pojedynków graficznie na wiele różnych sposobów.

Repozytorium przeniesione na Github.com, wysłano PullRequest

Link: [Axelrod](https://github.com/kjurgielajtis/Axelrod)